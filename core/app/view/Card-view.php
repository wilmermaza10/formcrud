<div class="col-md-12 col-lg-12">
  <div class="card mg-b-20">
    <div class="card-header">
      <h4 class="card-header-title">
        Validation Custom Forms
      </h4>
      <div class="card-header-btn">
        <a href="#" data-toggle="collapse" class="btn card-collapse" data-target="#collapse7" aria-expanded="true"><i class="ion-ios-arrow-down"></i></a>
        <a href="#" data-toggle="refresh" class="btn card-refresh"><i class="ion-android-refresh"></i></a>
        <a href="#" data-toggle="expand" class="btn card-expand"><i class="ion-android-expand"></i></a>
        <a href="#" data-toggle="remove" class="btn card-remove"><i class="ion-android-close"></i></a>
      </div>
    </div>
    <div class="card-body collapse show" id="collapse7">
      <form class="needs-validation" action="index.php?action=Agregar" method="post" novalidate>
        <div class="form-row">
          <div class="col-md-4 mb-3">
            <label for="validationCustom01">Nombre</label>
            <input type="text" class="form-control" id="validationCustom01" name="Nombre" required>
            <div class="valid-feedback">
              Looks good!
            </div>
          </div>
          <div class="col-md-4 mb-3">
            <label for="validationCustom02">Apellido</A></label>
            <input type="text" class="form-control" id="validationCustom02" name="Apellido"required>
            <div class="valid-feedback">
              Looks good!
            </div>
          </div>
          <div class="form-group">
            <label for="validationCustom01">Tip. Documento</label>
            <select class="custom-select" name="Documento" required>
              <option value="">Seleccione</option>
              <option value="TI">TI</option>
              <option value="CC">CC</option>
              <option value="TE">TE</option>
            </select>
            <div class="invalid-feedback">Example invalid custom select feedback</div>
          </div>
          <div class="form-row">
            <div class="col-md-4 mb-3">
              <label for="validationCustom01">No. Documento</label>
              <input type="text" class="form-control" id="validationCustom01" name="NO" required>
              <div class="valid-feedback">
                Looks good!
              </div>
            </div>
            <div class="card-body collapse show" id="collapse3">
              <div class="input-group">
                <input id="datePicker1" type="text" class="form-control datepicker-here" name="Espedicion"  placeholder="Fecha Expedicion">
                <div class="input-group-append">
                  <label for="datePicker1" class="input-group-text"><i class="fa fa-calendar"></i></label>
                </div>
              </div>
            </div>
            <div class="card-body collapse show" id="collapse3">
              <div class="input-group">
                <input id="datePicker1" type="text" class="form-control datepicker-here" name="Nacimiento" placeholder="Fecha Nacimiento">
                <div class="input-group-append">
                  <label for="datePicker1" class="input-group-text"><i class="fa fa-calendar"></i></label>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="validationCustom01">Sexo</label>
              <select class="custom-select" name="Sexo" required>
                <option value="">Seleccione</option>
                <option value="M">Hombre</option>
                <option value="F">Mujer</option>
                <option value="N">Otro</option>
              </select>
              <div class="invalid-feedback">Example invalid custom select feedback</div>
            </div>
            
            <button class="btn btn-custom-primary" type="submit">Submit form</button>
      </form>
    </div>
  </div>
</div>