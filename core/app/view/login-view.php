<div class="col-md-12 col-lg-12">
    <div class="card mg-b-20">
        <div class="card-header">
            <h4 class="card-header-title">
                Validation Custom Forms
            </h4>
            <div class="card-header-btn">
                <a href="#" data-toggle="collapse" class="btn card-collapse" data-target="#collapse7" aria-expanded="true"><i class="ion-ios-arrow-down"></i></a>
                <a href="#" data-toggle="refresh" class="btn card-refresh"><i class="ion-android-refresh"></i></a>
                <a href="#" data-toggle="expand" class="btn card-expand"><i class="ion-android-expand"></i></a>
                <a href="#" data-toggle="remove" class="btn card-remove"><i class="ion-android-close"></i></a>
            </div>
        </div>
        <div class="card-body collapse show" id="collapse7">


            <button onclick="location.href='./index.php?view=Card'" name="Formulario" class="btn btn-success btn-block"><i class="fa fa-download mg-r-10"></i> Formulario</button>
            <button onclick="location.href ='./index.php?view=Tabla'" name="Datos" class="btn btn-success btn-block"><i class="fa fa-download mg-r-10"></i> Datos</button>

        </div>
    </div>
</div>
