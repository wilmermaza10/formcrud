
<?php
$id = $_POST["id"];
$datos = FormularioData::getByNO($id);
?>
<div class="col-md-12 col-lg-12">
  <div class="card mg-b-20">
    <div class="card-header">
      <h4 class="card-header-title">
        Validation Custom Forms
      </h4>
      <div class="card-header-btn">
        <a href="#" data-toggle="collapse" class="btn card-collapse" data-target="#collapse7" aria-expanded="true"><i class="ion-ios-arrow-down"></i></a>
        <a href="#" data-toggle="refresh" class="btn card-refresh"><i class="ion-android-refresh"></i></a>
        <a href="#" data-toggle="expand" class="btn card-expand"><i class="ion-android-expand"></i></a>
        <a href="#" data-toggle="remove" class="btn card-remove"><i class="ion-android-close"></i></a>
      </div>
    </div>
    <div class="card-body collapse show" id="collapse7">
      <form class="needs-validation" action="index.php?action=Editar" method="post" novalidate>
        <div class="form-row">
          <div class="col-md-4 mb-3">
            <label for="validationCustom01">Nombre</label>
            <input type="text" class="form-control" id="validationCustom01" name="Nombre" value="<?php echo $datos->Nombre ?>" required>
            <div class="valid-feedback">
              Looks good!
            </div>
          </div>
          <div class="col-md-4 mb-3">
            <label for="validationCustom02">Apellido</A></label>
            <input type="text" class="form-control" id="validationCustom02" name="Apellido" value="<?php echo $datos->Apellido ?>" required>
            <div class="valid-feedback">
              Looks good!
            </div>
          </div>
          <div class="form-group">
            <label for="validationCustom01">Tip. Documento</label>
            <select class="custom-select" name="Documento" required>
              <option <?php if ($datos->TDocumento == "NN") {
                        echo "selected";
                      } ?> value="NN">Seleccione</option>
              <option <?php if ($datos->TDocumento == "TI") {
                        echo "Selected";
                      } ?> value="TI">TI</option>
              <option <?php if ($datos->TDocumento == "CC") {
                        echo "Selected";
                      } ?> value="CC">CC</option>
              <option <?php if ($datos->TDocumento == "TE") {
                        echo "Selected";
                      } ?> value="TE">TE</option>
            </select>
            <div class="invalid-feedback">Example invalid custom select feedback</div>
          </div>
          <div class="form-row">
            <div class="col-md-4 mb-3">
              <label for="validationCustom01">No. Documento</label>
              <input type="text" class="form-control" id="validationCustom01" value="<?php echo $datos->NO ?>" name="NO" required>
              <div class="valid-feedback">
                Looks good!
              </div>
            </div>
            <div class="card-body collapse show" id="collapse3">
              <div class="input-group">
                <input id="datePicker1" type="text" class="form-control datepicker-here" value="<?php echo $datos->FEs ?>" name="Espedicion" placeholder="Fecha Expedicion">
                <div class="input-group-append">
                  <label for="datePicker1" class="input-group-text"><i class="fa fa-calendar"></i></label>
                </div>
              </div>
            </div>
            <div class="card-body collapse show" id="collapse3">
              <div class="input-group">
                <input id="datePicker1" type="text" class="form-control datepicker-here" value="<?php echo $datos->FNa?>" name="Nacimiento" placeholder="Fecha Nacimiento">
                <div class="input-group-append">
                  <label for="datePicker1" class="input-group-text"><i class="fa fa-calendar"></i></label>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="validationCustom01">Sexo</label>
              <select class="custom-select" name="Sexo" required>
                <option <?php if ($datos->sexo == "NN") {
                  echo "Selected";
                }?>
                value="NN">Seleccione</option>
                <option <?php if ($datos->sexo == "M") {
                  echo "Selected";
                }?> 
                value="M">Hombre</option>
                <option <?php if ($datos->sexo == "F") {
                  echo "Selected";
                } ?>
                value="F">Mujer</option>
                <option <?php if ($datos->sexo== "N") {
                  echo"Selected";
                } ?>
                value="N">Otro</option>
              </select>
              <div class="invalid-feedback">Example invalid custom select feedback</div>
            </div>

            <button class="btn btn-custom-primary" type="submit">Submit form</button>
      </form>
    </div>
  </div>
</div>