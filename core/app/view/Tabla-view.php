<!-- Header Color Table Start -->
<!--================================-->
<div class="col-md-12 col-lg-12">
  <div class="card mg-b-20">
    <div class="card-header">
      <h4 class="card-header-title">
        Header Color Table
      </h4>
      <div class="card-header-btn">
        <a href="#" data-toggle="collapse" class="btn card-collapse" data-target="#collapse9" aria-expanded="true"><i class="ion-ios-arrow-down"></i></a>
        <a href="#" data-toggle="refresh" class="btn card-refresh"><i class="ion-android-refresh"></i></a>
        <a href="#" data-toggle="expand" class="btn card-expand"><i class="ion-android-expand"></i></a>
        <a href="#" data-toggle="remove" class="btn card-remove"><i class="ion-android-close"></i></a>
      </div>
    </div>
    <div class="card-body pd-0 collapse show" id="collapse9">
      <table class="table">
        <thead class="thead-colored thead-primary">
          <tr>

            <th class="wd-10p">Tipo Documento</th>
            <th class="wd-35p"> Numero</th>
            <th class="wd-35p">Nombre</th>
            <th class="wd-35p">Apellido</th>
            <th class="wd-35p">Fe. Nacimiento</th>
            <th class="wd-35p">Fe. Expedicion</th>
            <th class="wd-10p">Sexo</th>
            <th class="wd-30p">Accion </th>
            <th class="wd-30p"> </th>

          </tr>
        </thead>
        <tbody>

          <?php
          $datos = FormularioData::getAll();

          foreach ($datos as $dato) { ?>
            <tr>
              <td><?php echo $dato->TDocumento; ?></td>

              <td><?php echo $dato->NO; ?></td>

              <td><?php echo $dato->Nombre; ?></td>

              <td><?php echo $dato->Apellido; ?></td>

              <td><?php echo $dato->FNa; ?></td>

              <td><?php echo $dato->FEs; ?></td>

              <td><?php echo $dato->sexo; ?></td>

              <td>
                <form action="index.php?action=Eliminar" method="post">
                  <input type="hidden" name="id" value="<?php echo $dato->NO ?>">
                  <button type="submit">Eliminar</button>
                </form>
              </td>
              <td>
                <form action="index.php?view=Editar" method="post">

                  <input type="hidden" name="id" value="<?php echo $dato->NO ?>">
                  <button type=" submit">Editar</button>
                </form>
              </td>
            </tr>
          <?php
          }
          ?>

        </tbody>
     
      </table>

    </div>
  </div>
</div>
<!--/ Header Color Table End -->